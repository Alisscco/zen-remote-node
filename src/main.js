import express from 'express'
import bodyParser from 'body-parser'
import axios from 'axios'
import cors from 'cors'
import { isArray, every, isString, isEmpty, isNumber } from 'lodash'

const axiosInstance = axios.create({
    baseURL: 'http://' + (process.env.API || '127.0.0.1:11567'),
    timeout: 10000
})

const app = express()
app.use(bodyParser.json())
app.use(cors())

const get = function (url, response) {
    axiosInstance
        .get(url)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            console.log(error)
            response.sendStatus(500)
        })
}

const post = function (url, postData, response) {
    axiosInstance
        .post(url, postData)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            console.log(error)
            response.sendStatus(500)
        })
}

const error = function (message, response) {
    response.status(400).send(message)
}

app.post('/api/unspentoutputs', (request, response) => {
    const addresses = request.body

    if (isArray(addresses) && every(addresses, isString)) {
        post('/addressdb/outputs', {
            'addresses': addresses,
            'mode': 'unspentOnly'
        }, response)
    } else {
        error('array of addresses is expected', response)
    }
})

app.post('/api/history', (request,response) => {
    const {body} = request

    if (isEmpty(body.addresses) || !isArray(body.addresses) || !every(body.addresses, isString)) {
        error('addresses is missing or invalid array', response)
        return
    }

    if (!isNumber(body.skip)) {
        error('skip is missing', response)
        return
    }

    if (!isNumber(body.take)) {
        error('take is missing', response)
        return
    }

    post('/addressdb/transactions', {
        'addresses': body.addresses,
        'skip': body.skip,
        'take': body.take
    }, response)
})

app.post('/api/publishtransaction', (request,response) => {
    const {tx} = request.body

    if (isEmpty(tx) || !isString(tx)) {
        error('expecting a tx field with base16 string', response)
        return
    }

    post('/blockchain/publishtransaction', {tx}, response)
})

app.post('/api/runcontract', (request, response) => {
    const {body} = request

    if (isEmpty(body.address) || !isString(body.address)) {
        error('address is missing', response)
        return
    }

    if (isEmpty(body.tx) || !isString(body.tx)) {
        error('tx is missing', response)
        return
    }

    let sender = ''

    if (!isEmpty(body.options)) {
        if (!isEmpty(body.options.sender)) {
            sender = body.options.sender
        }
    }

    post('/blockchain/contract/execute', {
        'address': body.address,
        'command': body.command,
        'messageBody': body.messageBody,
        'options': {
            'sender': sender
        },
        'tx': body.tx
    }, response)
})

app.get('/api/activecontracts', (request,response) => {
    get('/contract/active', response)
})

app.get('/api/info', (request,response) => {
    get('/blockchain/info', response)
})

app.listen(3000)
